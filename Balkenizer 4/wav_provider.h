#pragma once

#include "pcm_provider.h"

class wav_provider : public pcm_provider {
	public:
	explicit wav_provider(HANDLE input);

	// Don't move/copy
	wav_provider(const wav_provider&) = delete;
	wav_provider& operator=(const wav_provider&) = delete;
	wav_provider(wav_provider&&) = delete;
	wav_provider& operator=(wav_provider&&) = delete;
	
	virtual ~wav_provider() override;

	virtual sample_type pcm_type() const override;
	virtual uint64_t sample_count() const override;
	virtual uint16_t channel_count() const override;
	virtual uint64_t samples(void* dest, uint64_t n) const override;

	protected:
	virtual bool is_valid() const override;
	
	private:

#pragma pack(push, 1)
	// RIFF header structures
	struct riff_chunk {
		char id[4];
		uint32_t size;
	};
	
	struct format_chunk {
		riff_chunk chk;
		uint16_t audio_format;
		uint16_t num_channels;
		uint32_t sample_rate;
		uint32_t byte_rate;
		uint16_t block_align;
		uint16_t bits_per_sample;
	};
	
	struct riff_header {
		riff_chunk chk;
		char format[4];
	};

#pragma pack(pop)

	void _read();
	
	// File handle
	HANDLE _m_handle;

	riff_header _m_riff;
	format_chunk _m_fmt;
	riff_chunk _m_data;
	uint64_t _m_data_start;
	sample_type _m_sample_type;

	// Valid file?
	bool _m_valid;
};

