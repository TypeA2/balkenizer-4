﻿#include "startup_window.h"
#include "main_window.h"
#include "pcm_provider.h"
#include "utils.h"

#include <CommCtrl.h>
#include <ShObjIdl.h>

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR lpCmdLine,
	_In_ int nShowCmd) {
	(void) hPrevInstance;
	(void) lpCmdLine;

	INITCOMMONCONTROLSEX ex {
		sizeof(INITCOMMONCONTROLSEX),
		ICC_BAR_CLASSES	| ICC_COOL_CLASSES		| ICC_LISTVIEW_CLASSES	|
		ICC_NATIVEFNTCTL_CLASS	| ICC_PROGRESS_CLASS	| ICC_STANDARD_CLASSES	|
		ICC_TAB_CLASSES			| ICC_TREEVIEW_CLASSES	| ICC_UPDOWN_CLASS		|
		ICC_USEREX_CLASSES		| ICC_WIN95_CLASSES
	};

	InitCommonControlsEx(&ex);

	// COM
	HRESULT hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);

	if (!SUCCEEDED(hr)) {
		MessageBox(nullptr,
			TEXT("Failed to initialize COM"),
			TEXT("Startup error"),
			MB_ICONWARNING | MB_OK);
		return false;
	}

#if 0
	// Retrieve input file
	startup_window startup(hInstance, nShowCmd);
	int exit_code = startup.run();

	const TCHAR* selected_file;
	if ((selected_file = startup.result()) == nullptr) {
		return exit_code;
	}
#else
	int exit_code = EXIT_SUCCESS;
	const TCHAR* selected_file = TEXT("C:\\Users\\Nuan\\Downloads\\24-灼け落ちない翼.wav");
#endif
	
	pcm_provider* pcm = pcm_provider::create(selected_file);

	if (pcm) {
		main_window wmain(hInstance, nShowCmd);
		exit_code = wmain.run();
	} else {
		MessageBox(nullptr, TEXT("Unsupported file type"), TEXT("Error"), 
			MB_ICONWARNING | MB_OK);
		exit_code = EXIT_FAILURE;
	}

	CoUninitialize();
	
	return exit_code;
}
