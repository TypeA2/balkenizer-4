#pragma once

#include "frameworks.h"

class pcm_provider;

class main_window {
	public:
	main_window(HINSTANCE inst, int show_cmd);
	~main_window();

	void set_pcm(pcm_provider* pcm);

	int run() const;

	private:

	bool _init(int show_cmd);
	bool _register_class();
	bool _init_instance(int show_cmd);

	// Forwarded class WndProc function
	LRESULT _wnd_proc(HWND hwnd, UINT msg,
		WPARAM wparam, LPARAM lparam);

	// Forwards the message processing to the member function
	static LRESULT CALLBACK _wnd_proc_fwd(HWND hwnd, UINT msg,
		WPARAM wparam, LPARAM lparam);

	// String size
	static constexpr size_t STRING_SIZE = 255;

	// Strings
	TCHAR _m_title[STRING_SIZE];
	TCHAR _m_window_class[STRING_SIZE];
	
	bool _m_success;
	
	pcm_provider* _m_pcm;

	// Windows API stuff
	HINSTANCE _m_inst;
	ATOM _m_wclass;
	HWND _m_hwnd;

	HBRUSH _m_window_bg;
};

