#include "wav_provider.h"
#include "utils.h"

wav_provider::wav_provider(HANDLE input)
	: _m_handle(input), _m_valid(false) {
	_read();
}

wav_provider::~wav_provider() {
	CloseHandle(_m_handle);
}

pcm_provider::sample_type wav_provider::pcm_type() const {
	return sample_type_count;
}

uint64_t wav_provider::sample_count() const {
	return _m_valid ?
		((_m_data.size / _m_fmt.num_channels)
			/ (_m_fmt.bits_per_sample / 8))
			: 0;
}

uint16_t wav_provider::channel_count() const {
	return _m_valid ? _m_fmt.num_channels : 0;
}


uint64_t wav_provider::samples(void* dest, uint64_t n) const {
	if (!_m_valid) {
		return 0;
	}
	(void) dest;
	(void) n;
	return n;
}

bool wav_provider::is_valid() const {
	return _m_valid;
}

void wav_provider::_read() {
	DWORD high;
	uint64_t file_size = GetFileSize(_m_handle, &high);
	file_size |= (uint64_t(high) << (sizeof(high) * 8));
	
	DWORD read = 0;
	if (!ReadFile(_m_handle, &_m_riff, 
		sizeof(_m_riff), &read, nullptr)
		|| read != sizeof(_m_riff)) {
		return;
	}

	// Not RIFF
	if (memcmp(_m_riff.chk.id, "RIFF", 4) != 0) {
		return;
	}

	// Not WAVE
	if (memcmp(_m_riff.format, "WAVE", 4) != 0) {
		return;
	}

	riff_chunk hdr;
	uint64_t pos = 0;
	while (pos != file_size) {
		// Read chunks
		if (!ReadFile(_m_handle, &hdr, sizeof(hdr), &read, nullptr)
			|| read != sizeof(hdr)) {
			return;
		}

		if (memcmp(hdr.id, "fmt ", 4) == 0) {
			// fmt chunk
			memcpy(&_m_fmt.chk, &hdr, sizeof(hdr));
			if (!ReadFile(_m_handle, &_m_fmt.audio_format,
				sizeof(_m_fmt) - sizeof(_m_fmt.chk), &read, nullptr)
				|| read != (sizeof(_m_fmt) - sizeof(_m_fmt.chk))) {
				return;
			}

			// Must be integer PCM
			if (_m_fmt.chk.size != 16) {
				return;
			}
			if (_m_fmt.audio_format != 1) {
				return;
			}

			// ByteRate and BlockAlign must be correct
			if (_m_fmt.byte_rate !=
				(_m_fmt.sample_rate * _m_fmt.num_channels * (_m_fmt.bits_per_sample / 8))) {
				return;
			}

			if (_m_fmt.block_align != (_m_fmt.num_channels * (_m_fmt.bits_per_sample / 8))) {
				return;
			}

			// Support bits per sample
			switch (_m_fmt.bits_per_sample) {
				case 8: _m_sample_type = uint8; break;
				case 16: _m_sample_type = int16; break;
				case 24: _m_sample_type = int24; break;
				case 32: _m_sample_type = int32; break;
				default: return;
			}
		} else if (memcmp(hdr.id, "data", 4) == 0) {
			memcpy(&_m_data, &hdr, sizeof(hdr));
			_m_data_start = SetFilePointer(_m_handle, 0, nullptr, FILE_CURRENT);
			SetFilePointer(_m_handle, hdr.size, nullptr, FILE_CURRENT);
		} else {
			// Skip chunk
			SetFilePointer(_m_handle, hdr.size, nullptr, FILE_CURRENT);
		}

		pos = SetFilePointer(_m_handle, 0, nullptr, FILE_CURRENT);
	}

	_m_valid = true;
}

