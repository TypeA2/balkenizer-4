#pragma once

#include "frameworks.h"

class startup_window {
	public:
	startup_window(HINSTANCE inst, int show_cmd);
	~startup_window();

	// Program loop
	int run() const;

	// Selected file
	LPTSTR result() const;

	private:
	// String size
	static constexpr size_t STRING_SIZE = 255;

	// Initialise stuff needed for the Windows API
	bool _init(int show_cmd);
	bool _register_class();
	bool _init_instance(int show_cmd);

	// Forwarded class WndProc function
	LRESULT _wnd_proc(HWND hwnd, UINT msg,
		WPARAM wparam, LPARAM lparam);

	// Forwards the message processing to the member function
	static LRESULT CALLBACK _wnd_proc_fwd(HWND hwnd, UINT msg,
		WPARAM wparam, LPARAM lparam);

	// Static, doesn't require the member variables
	static INT_PTR CALLBACK _about(HWND hwnd, UINT msg,
		WPARAM wparam, LPARAM lparam);

	// Paint routine
	LRESULT _paint() const;

	// Resize routine
	LRESULT _resize(WPARAM wparam, LPARAM lparam) const;

	// Start button clicked
	LRESULT _start();

	
	// Whether init succeeded
	bool _m_success;

	// Main instance
	HINSTANCE _m_inst;

	// Handles
	HWND _m_hwnd;
	HWND _m_start_button;

	HBRUSH _m_window_bg;
	HBRUSH _m_start_button_brush;

	HFONT _m_start_button_font;

	// Strings
	TCHAR _m_title[STRING_SIZE];
	TCHAR _m_window_class[STRING_SIZE];

	// Accelerators
	HACCEL _m_accel;

	// Result file
	LPTSTR _m_in_file;
};
