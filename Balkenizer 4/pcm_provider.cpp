#include "pcm_provider.h"

#include "wav_provider.h"

#include "utils.h"

pcm_provider* pcm_provider::create(const TCHAR* input_file) {

	HANDLE hfile = CreateFile(
		input_file, GENERIC_READ, FILE_SHARE_READ,
		nullptr, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL, nullptr);

	if (hfile == INVALID_HANDLE_VALUE) {
		DWORD error = GetLastError();
		LPTSTR error_message;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			nullptr, error, 0,
			reinterpret_cast<LPTSTR>(&error_message), 0, nullptr);

		utils::coutln("Could not open input file:", error_message);
		
		LocalFree(error_message);

		return nullptr;
	}

	std::string fourcc(4, '\0');
	DWORD read = 0;
	if (!ReadFile(hfile, fourcc.data(), 4, &read, nullptr)
		|| read != 4) {
		utils::coutln("Could not read fourcc");

		CloseHandle(hfile);
		return nullptr;
	}

	pcm_provider* pcm;

	if (fourcc == "RIFF") {
		// RIFF size
		SetFilePointer(hfile, 4, nullptr, FILE_CURRENT);
		(void) ReadFile(hfile, fourcc.data(), 4, &read, nullptr);

		if (fourcc == "WAVE") {
			SetFilePointer(hfile, 0, nullptr, FILE_BEGIN);
			pcm = new wav_provider(hfile);

			if (!pcm->is_valid()) {
				delete pcm;
				return nullptr;
			}

			return pcm;
		}
	}

	CloseHandle(hfile);
	return nullptr;
}

// ReSharper disable once hicpp-use-equals-default
pcm_provider::~pcm_provider() {
	// dummy
}


