#include "startup_window.h"

#include "Resource.h"
#include "utils.h"

#include <cstdlib>
#include <CommCtrl.h>
#include <ShObjIdl.h>
#include <strsafe.h>

startup_window::startup_window(HINSTANCE inst, int show_cmd)
	: _m_success(true)
	, _m_inst(inst)
	, _m_in_file(nullptr) {
	// Whether the setup was a success
	_m_success = _init(show_cmd);
}

startup_window::~startup_window() {
	delete[] _m_in_file;
}


int startup_window::run() const {
	// Exit on failure
	if (!_m_success) {
		return EXIT_FAILURE;
	}

	// Process messages
	MSG msg;

	BOOL ret;
	while ((ret = GetMessage(&msg, nullptr, 0, 0)) != 0) {
		if (ret == -1) {
			return EXIT_FAILURE;
		} 

		if (!TranslateAccelerator(msg.hwnd, _m_accel, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return int(msg.wParam);
}

LPTSTR startup_window::result() const {
	return _m_in_file;
}

bool startup_window::_init(int show_cmd) {
	// Load string resources
	LoadString(_m_inst, IDS_APP_TITLE, _m_title, STRING_SIZE);
	LoadString(_m_inst, IDC_START_WINDOW, _m_window_class, STRING_SIZE);

	// Additional setup
	if (!_register_class() || !_init_instance(show_cmd)) {
		return false;
	}

	// Load accelerators
	_m_accel = LoadAccelerators(_m_inst, MAKEINTRESOURCE(IDC_START_WINDOW));

	return _m_accel != nullptr;
}

bool startup_window::_register_class() {
	_m_window_bg = CreateSolidBrush(RGB(0xf0, 0xf0, 0xf0));
	
	// Our window class instance
	WNDCLASSEX wcex {
		sizeof(WNDCLASSEX),
		CS_HREDRAW | CS_VREDRAW,
		_wnd_proc_fwd,
		0,
		0,
		_m_inst,
		LoadIcon(_m_inst, MAKEINTRESOURCE(IDI_BALKENIZER4)),
		LoadCursor(nullptr, IDC_ARROW),
		_m_window_bg,
		MAKEINTRESOURCE(IDC_START_WINDOW),
		_m_window_class,
		LoadIcon(_m_inst, MAKEINTRESOURCE(IDI_BALKENIZER4))
	};

	return RegisterClassEx(&wcex) != 0;
}

bool startup_window::_init_instance(int show_cmd) {
	// Create the main window
	_m_hwnd = CreateWindow(
		_m_window_class, _m_title, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 960, 720,
		nullptr, nullptr, _m_inst, nullptr);

	if (!_m_hwnd) {
		return false;
	}

	// Set user data to a pointer to this instance
	SetWindowLongPtr(_m_hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

	// Center the window
	RECT rc;
	GetWindowRect(_m_hwnd, &rc);

	SetWindowPos(_m_hwnd, nullptr,
		(GetSystemMetrics(SM_CXSCREEN) - rc.right) / 2,
		(GetSystemMetrics(SM_CYSCREEN) - rc.bottom) / 2, 
		0, 0, SWP_NOZORDER | SWP_NOSIZE);

	// Controls

	// Main button
	_m_start_button = CreateWindow(
		TEXT("BUTTON"),
		TEXT("Start Balkenizing"),
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON | BS_OWNERDRAW,
		0, 0, 320, 72,
		_m_hwnd, HMENU(IDC_STARTBUTTON), _m_inst, nullptr);

	SetClassLongPtr(_m_start_button, GCLP_HCURSOR,
		reinterpret_cast<LONG_PTR>(LoadCursor(nullptr, IDC_HAND)));

	_m_start_button_brush = CreateSolidBrush(RGB(0x00, 0x80, 0xff));
	_m_start_button_font = HFONT(GetStockObject(DEFAULT_GUI_FONT));
	LOGFONT fnt;
	GetObject(_m_start_button_font, sizeof(fnt), &fnt);
	fnt.lfHeight = -18;
	_m_start_button_font = CreateFontIndirect(&fnt);

	// Continue
	ShowWindow(_m_hwnd, show_cmd);
	UpdateWindow(_m_hwnd);
	return true;
}

LRESULT startup_window::_wnd_proc(HWND hwnd, UINT msg,
	WPARAM wparam, LPARAM lparam) {
	if (hwnd != _m_hwnd) {
		utils::coutln("Wrong handle");
		return -1;
	}
	
	// Message processing
	switch (msg) {
		case WM_COMMAND: {
			switch (LOWORD(wparam)) {
				case IDM_ABOUT:
					DialogBox(_m_inst, MAKEINTRESOURCE(IDD_ABOUTBOX), hwnd, _about);
					break;
				
				case IDM_EXIT:
					DestroyWindow(hwnd);
					break;
				
				case IDC_STARTBUTTON:
					return _start();
					break;
				
				default:
					return DefWindowProc(hwnd, msg, wparam, lparam);
			}
			break;
		}
		
		case WM_CTLCOLORBTN: {
			HDC hdc = reinterpret_cast<HDC>(wparam);
			HWND handle = reinterpret_cast<HWND>(lparam);

			if (handle == _m_start_button) {
				RECT rc { 0, 0, 320, 72 };

				SetTextColor(hdc, RGB(0xfa, 0xfa, 0xfa));
				SetBkMode(hdc, TRANSPARENT);
				SelectObject(hdc, _m_start_button_font);
				DrawText(hdc, TEXT("Start Balkenizing"), -1, &rc,
					DT_CENTER | DT_VCENTER | DT_SINGLELINE);

				return LONGLONG(_m_start_button_brush);
			}
			break;
		}

		case WM_PAINT:
			return _paint();

		case WM_SIZE:
			return _resize(wparam, lparam);

		case WM_GETMINMAXINFO: {
			LPMINMAXINFO mmi = reinterpret_cast<LPMINMAXINFO>(lparam);
			mmi->ptMinTrackSize.x = 480;
			mmi->ptMinTrackSize.y = 480;
			break;
		}

		case WM_CLOSE:
			DestroyWindow(_m_start_button);
			DestroyWindow(_m_hwnd);
			return 0;
		
		case WM_DESTROY: {
			DeleteObject(_m_window_bg);
			DeleteObject(_m_start_button_brush);
			DeleteObject(_m_start_button_font);

			PostQuitMessage(EXIT_SUCCESS);
			break;
		}

		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	return 0;
}

INT_PTR startup_window::_about(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	(void) lparam;

	// Simpler
	switch (msg) {
		case WM_INITDIALOG:
			return 1;

		case WM_COMMAND:
			if (LOWORD(wparam) == IDOK || LOWORD(wparam) == IDCANCEL) {
				EndDialog(hwnd, LOWORD(wparam));
				return 1;
			}

		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
	}
}

LRESULT startup_window::_wnd_proc_fwd(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	// Retrieve the instance pointer
	startup_window* _this = reinterpret_cast<startup_window*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

	if (_this) {
		// Forward processing to the instance
		return _this->_wnd_proc(hwnd, msg, wparam, lparam);
	}

	return DefWindowProc(hwnd, msg, wparam, lparam);
}

LRESULT startup_window::_paint() const {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(_m_hwnd, &ps);
	
	(void) hdc;
	
	EndPaint(_m_hwnd, &ps);

	return 0;
}

LRESULT startup_window::_resize(WPARAM wparam, LPARAM lparam) const {
	(void) wparam;
	
	WORD new_width = LOWORD(lparam);
	WORD new_height = HIWORD(lparam);

	// Start button
	RECT rc;
	GetWindowRect(_m_start_button, &rc);

	// This causes the "The handle is invalid" spam
	SetWindowPos(_m_start_button, _m_hwnd,
		(new_width - (rc.right - rc.left)) / 2,
		(new_height - (rc.bottom - rc.top)) / 2,
		0, 0, SWP_NOZORDER | SWP_NOSIZE);
	
	return 0;
}

LRESULT startup_window::_start() {

	IFileOpenDialog* file_open;
	
	HRESULT hr = CoCreateInstance(CLSID_FileOpenDialog, nullptr, CLSCTX_ALL,
		IID_IFileOpenDialog, reinterpret_cast<void**>(&file_open));

	if (FAILED(hr)) {
		utils::coutln("Failed to create FileOpenDialog");
		return 0;
	}

	COMDLG_FILTERSPEC filters[1] = {
		{ TEXT("WAV audio"), TEXT("*.wav") }
	};

	hr = file_open->SetFileTypes(sizeof(filters) / sizeof(COMDLG_FILTERSPEC), filters);

	if (FAILED(hr)) {
		utils::coutln("Failed to set file filters");
	} else {
		hr = file_open->Show(_m_hwnd);

		if (FAILED(hr)) {
			// Closed without doing anything
			return 0;
		}

		IShellItem* item;
		hr = file_open->GetResult(&item);

		if (FAILED(hr)) {
			utils::coutln("Failed to get selected item");
		} else {
			PTSTR file_path;
			hr = item->GetDisplayName(SIGDN_FILESYSPATH, &file_path);

			if (FAILED(hr)) {
				utils::coutln("Failed to get file path");
			} else {
				size_t length;
				hr = StringCchLength(file_path, STRSAFE_MAX_CCH, &length);

				if (hr != S_OK) {
					utils::coutln("Failed to calculate string length");
					return 0;
				}

				_m_in_file = new TCHAR[length + 1];
				hr = StringCchCopy(_m_in_file, length + 1, file_path);

				if (hr != S_OK) {
					utils::coutln("Failed to copy file path");
					return 0;
				}

				CoTaskMemFree(file_path);

				DestroyWindow(_m_hwnd);
			}

			item->Release();
		}
	}
	
	file_open->Release();

	return 0;
}

