#pragma once

#include "frameworks.h"

#include <cstdint>

class pcm_provider {
	public:
	static pcm_provider* create(const TCHAR* input_file);

	virtual ~pcm_provider() = 0;

	pcm_provider(const pcm_provider&) = delete;
	pcm_provider& operator=(const pcm_provider&) = delete;
	
	pcm_provider(pcm_provider&&) = delete;
	pcm_provider& operator=(const pcm_provider&&) = delete;
	
	enum sample_type {
		uint8,  int8,
		uint16, int16,
		uint24, int24,
		uint32, int32,
		uint64, int64,
		float32, float64,
		sample_type_count
	};

	static constexpr uint8_t sample_size[sample_type_count] = {
		1, 1, 2, 2, 3, 3, 4, 4, 8, 8, 4, 8
	};

	// Type of PCM data provided
	virtual sample_type pcm_type() const = 0;

	// Number of samples per channel
	virtual uint64_t sample_count() const = 0;

	// Number of channels
	virtual uint16_t channel_count() const = 0;

	// Retrieve n samples per channel, returns the actual number of samples written per channel
	virtual uint64_t samples(void* dest, uint64_t n) const = 0;

	protected:
	pcm_provider() = default;

	// If the instance is valid
	virtual bool is_valid() const = 0;
};

