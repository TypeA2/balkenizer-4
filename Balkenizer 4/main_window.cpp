#include "main_window.h"

#include "pcm_provider.h"
#include "utils.h"

#include "Resource.h"

#include <cstdlib>

main_window::main_window(HINSTANCE inst, int show_cmd)
	: _m_success(false)
	, _m_inst(inst) {
	_m_success = _init(show_cmd);
}

main_window::~main_window() {
	delete _m_pcm;
}

void main_window::set_pcm(pcm_provider* pcm) {
	_m_pcm = pcm;
}

int main_window::run() const {
	if (!_m_success) {
		return EXIT_FAILURE;
	}

	MSG msg;

	BOOL ret;
	while ((ret = GetMessage(&msg, nullptr, 0, 0)) != 0) {
		if (ret == -1) {
			return EXIT_FAILURE;
		}

		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return int(msg.wParam);
}

bool main_window::_init(int show_cmd) {
	LoadString(_m_inst, IDS_APP_TITLE, _m_title, STRING_SIZE);
	LoadString(_m_inst, IDC_MAIN_WINDOW, _m_window_class, STRING_SIZE);

	if (!_register_class() || !_init_instance(show_cmd)) {
		utils::coutln(_m_wclass);
		DWORD error = GetLastError();
		LPTSTR error_message;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			nullptr, error, 0,
			reinterpret_cast<LPTSTR>(&error_message), 0, nullptr);

		utils::coutln(error, error_message);

		LocalFree(error_message);
		return false;
	}

	return true;
}

bool main_window::_register_class() {
	_m_window_bg = CreateSolidBrush(RGB(0xf0, 0xf0, 0xf0));

	WNDCLASSEX wcex {
		sizeof(WNDCLASSEX),
		CS_HREDRAW | CS_VREDRAW,
		_wnd_proc_fwd,
		0,
		0,
		_m_inst,
		LoadIcon(_m_inst, MAKEINTRESOURCE(IDI_BALKENIZER4)),
		LoadCursor(nullptr, IDC_ARROW),
		_m_window_bg,
		nullptr,
		_m_window_class,
		LoadIcon(_m_inst, MAKEINTRESOURCE(IDI_BALKENIZER4))
	};

	return (_m_wclass = RegisterClassEx(&wcex)) != 0;
}

bool main_window::_init_instance(int show_cmd) {
	_m_hwnd = CreateWindow(
		reinterpret_cast<LPTSTR>(_m_wclass), _m_title, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		nullptr, nullptr, _m_inst, nullptr);
	
	if (!_m_hwnd) {
		return false;
	}

	SetWindowLongPtr(_m_hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

	// Maybe use it later
	(void) show_cmd;
	ShowWindow(_m_hwnd, SW_MAXIMIZE);
	UpdateWindow(_m_hwnd);
	return true;
}

LRESULT main_window::_wnd_proc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	if (hwnd != _m_hwnd) {
		utils::coutln("Wrong handle");
		return -1;
	}

	switch (msg) {
		case WM_CLOSE:
			DestroyWindow(_m_hwnd);
			return 0;

		case WM_DESTROY: {
			DeleteObject(_m_window_bg);

			PostQuitMessage(EXIT_SUCCESS);
			break;
		}

		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	return 0;
}


LRESULT main_window::_wnd_proc_fwd(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	// Retrieve the instance pointer
	main_window* _this = reinterpret_cast<main_window*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

	if (_this) {
		// Forward processing to the instance
		return _this->_wnd_proc(hwnd, msg, wparam, lparam);
	}

	return DefWindowProc(hwnd, msg, wparam, lparam);
}

